let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Host = new Schema(
  {

    connection: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    name: String,
    image: String,
    email: String,

  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Host', Host);
