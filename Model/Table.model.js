let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Table = new Schema(
  {

    noSeats: Number,
    arrivedUser: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    invitedUsers: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    items: [{
      type: Schema.Types.ObjectId,
      ref: 'Item'
    }],
    host: {
      type: Schema.Types.ObjectId,
      ref: 'Host'
    },
    isVisible: String,
    isBoosted: String,
    isBooked: String
  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Table', Table);
