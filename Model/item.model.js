let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Item = new Schema(
  {

    name: String,
    dtype: {
      type: String,
      enum: ['Food', 'Alcohol']
    }
  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Item', Item);
