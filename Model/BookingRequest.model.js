let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let BookRequesting = new Schema(
  {

    toUser: {
      type: Schema.Types.ObjectId,
      ref: 'Tags'
    },
    fromUser: {
      type: Schema.Types.ObjectId,
      ref: 'Tags'
    },
    status: ['pending', 'rejected', 'approved']

  },
  {
    timestamps: true
  });
module.exports = mongoose.model('BookRequesting', BookRequesting);
