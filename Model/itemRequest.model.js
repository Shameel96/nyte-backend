let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let ItemRequested = new Schema(
  {

    toHost: {
      type: Schema.Types.ObjectId,
      ref: 'Host'
    },
    toOwner: {
      type: Schema.Types.ObjectId,
      ref: 'Owner'
    },
    fromUser: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    fromHost:
    {
      type: Schema.Types.ObjectId,
      ref: 'Host'
    },
    items: [{
      type: Schema.Types.ObjectId,
      ref: 'Item'
    }],
  },
  {
    timestamps: true
  });
module.exports = mongoose.model('ItemRequested', ItemRequested);
