let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Feedback = new Schema(
  {
    toUser: {
      type: Schema.Types.ObjectId,
      ref: 'Owner'
    },
    toEvent: {
      type: Schema.Types.ObjectId,
      ref: 'Event'
    },
    toHost: {
      type: Schema.Types.ObjectId,
      ref: 'Host'
    },
    toOwner: {
      type: Schema.Types.ObjectId,
      ref: 'Owner'
    },
    fromUser:
    {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    fromHost:
    {
      type: Schema.Types.ObjectId,
      ref: 'Host'
    },
    fromOwner:
    {
      type: Schema.Types.ObjectId,
      ref: 'Owner'
    },
    message: String,


  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Feedback', Feedback);
