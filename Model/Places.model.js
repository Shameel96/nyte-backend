let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Places = new Schema(
  {
    name: String,
    lat: String,
    lng: String,
    image: [{ type: String }],
    address: String,
    tables: [{
      type: Schema.Types.ObjectId,
      ref: 'Table'
    }],

  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Places', Places);
