let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Owner = new Schema(
  {

    places: [{
      type: Schema.Types.ObjectId,
      ref: 'Places'
    }],
    name: String,
    image: String,
    email: String,
    connection: []

  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Owner', Owner);
