let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let User = new Schema(
  {

    image: String,
    userConnections: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    hostConnections: [{
      type: Schema.Types.ObjectId,
      ref: 'Host'
    }],
    ownerConnections: [{
      type: Schema.Types.ObjectId,
      ref: 'Owner'
    }],
    name: String,
    email: String,
    tags: [{
      type: Schema.Types.ObjectId,
      ref: 'Tags'
    }]

  },
  {
    timestamps: true
  });
module.exports = mongoose.model('User', User);
