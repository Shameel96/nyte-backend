let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Event = new Schema(
  {
    name: String,
    place: {
      type: Schema.Types.ObjectId,
      ref: 'Places'
    },
    startTime: Date,
    host: {
      type: Schema.Types.ObjectId,
      ref: 'Host'
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'Owner'
    },
    userAcceptd: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    userRejected: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    userPending: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    isVisible: String,
    isBoosted: String,
    dtype: ['Club, Restaurant, Bar/Lounge, Private Home, Hotel Room, Private Jet, Yatch, Other']
  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Event', Event);
