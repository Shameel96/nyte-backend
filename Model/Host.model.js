let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let Host = new Schema(
  {

    userConnection: [{
      type: Schema.Types.ObjectId,
      ref: 'User'
    }],
    hostConnection: [{
      type: Schema.Types.ObjectId,
      ref: 'Host'
    }],
    ownerConnection: [{
      type: Schema.Types.ObjectId,
      ref: 'Owner'
    }],
    name: String,
    image: String,
    email: String,





  },
  {
    timestamps: true
  });
module.exports = mongoose.model('Host', Host);
