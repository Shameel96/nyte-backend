let express = require('express');
let path = require('path');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let passport = require('passport');
let cors = require('cors');
let http = require('http');
const port = 3000
let app = express();
let placesRouter = require('./Features/Places/places.routes');
let itemRouter = require('./Features/Item/item.routes');
let eventRouter = require('./Features/Event/Event.routes');
mongoose.connect('mongodb://localhost:27017/test', {
  reconnectInterval: 5000,
  reconnectTries: 60
});
let db = mongoose.connection;
db.on(`error`, console.error.bind(console, `connection error:`));
db.once(`open`, function () {
  // we`re connected!
  console.log(`MongoDB connected on "  mongodb://localhost:27017/nyte`);
  console.log(`###########################################################################`);
});
app.set(`views`, path.join(__dirname, `views`));
app.set(`view engine`, `jade`);
app.use(cors());
app.use(logger(`dev`));
app.use(bodyParser.json({ limit: '100mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, `public`)));

//app.get('/', (req, res) => res.send('Hello World!'));
app.use('/place', placesRouter);
app.use('/event', eventRouter);
app.use('/item', itemRouter);
app.set(`port`, port);

let server = http.createServer(app);
server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
  console.log(`server is listening on ${port}`)
});
app.listen(3000, '0.0.0.0', function () {
  console.log('Listening to port:  ' + 3000);
});
// app.listen(port, () => console.log(`Example app listening on port ${port}!`))