let express = require('express');

let router = express.Router();

let eventCrtl = require('./Event.controller');
/**
 * @api {post} /event/radius Request Get Event By Radius
 * @apiName getEventByRadius
 * @apiGroup Event
 *
 * @apiParam {lat} lat latitude of place.
 * @apiParam {lng} lng  latitude of place.
 * @apiParam {radius} radius of palce.
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *     {
 *       "firstname": "John",
 *       "lastname": "Doe"
 *     }
 */
router.post('/radius', eventCrtl.getEventByRadius);
router.post('/', eventCrtl.registerEvents);
router.get('/', eventCrtl.getEvent);


module.exports = router;
