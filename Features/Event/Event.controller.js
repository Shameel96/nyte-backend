let Events = require('../../Model/Event.model');
let moongose = require('mongoose');

exports.registerEvents = function (req, res, next) {
  let newEvent = new Events({
    name: req.body.name,
    place: req.body.place
  });
  newEvent.save((err, data) => {
    if (err) {
      return res.json({
        message: "Getting Error",
        success: false,
        data: null
      });
    }
    return res.json({
      message: "Event Has Been Added",
      success: true,
      data: data
    });
  })
}
exports.getEvent = function (req, res) {
  Events.find({})
    .populate('place')
    .exec((err, data) => {
      if (err) {
        return res.json({
          message: "Getting Error",
          success: false,
          data: err
        });
      }
      return res.json({
        message: "All Event are there",
        success: true,
        data: data
      });
    });
}






function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  let R = 6371; // Radius of the earth in km
  let dLat = deg2rad(lat2 - lat1);  // deg2rad below
  let dLon = deg2rad(lon2 - lon1);
  let a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}


exports.getEventByRadius = function (req, res) {
  Events.find({})
    .populate('place')
    .exec((err, data) => {
      if (err) {
        return res.json({
          message: "Getting Error",
          success: false,
          data: err
        });
      }
      let events = [];
      for (let i = 0; i < data.length; i++) {
        let distanceInKilometer = getDistanceFromLatLonInKm(data[i].place.lat, data[i].place.lng, req.body.lat, req.body.lng);
        console.log(distanceInKilometer);
        if (distanceInKilometer < req.body.radius) {
          events.push(data[i]);
        }
      }
      return res.json({
        message: "Events Around Your",
        success: false,
        data: events
      });
    });
}
