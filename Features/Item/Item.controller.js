let Items = require('../../Model/item.model');
let moongose = require('mongoose');

exports.registerItem = function (req, res, next) {
  let newItem = new Items({
    name: req.body.name,
    dtype: req.body.dtype
  });
  newItem.save((err, data) => {
    if (err) {
      return res.json({
        message: "Getting Error",
        success: false,
        data: null
      });
    }
    return res.json({
      message: "newItem Has Been Added",
      success: true,
      data: data
    });
  })
}
exports.getPlaces = function (req, res) {
  Places.find({}, (err, data) => {
    if (err) {
      return res.json({
        message: "Getting Error",
        success: false,
        data: null
      });
    }
    return res.json({
      message: "All Places are there",
      success: true,
      data: data
    });
  });
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  let R = 6371; // Radius of the earth in km
  let dLat = deg2rad(lat2 - lat1);  // deg2rad below
  let dLon = deg2rad(lon2 - lon1);
  let a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  let d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI / 180)
}



exports.getPlaceByRadius = function (req, res) {
  Places.find({}, (err, data) => {
    if (err) {
      return res.json({
        message: "Getting Error",
        success: false,
        data: null
      });
    }
    let placesAroundYour = [];
    for (let i = 0; i < data.length; i++) {
      let distanceInKilometer = getDistanceFromLatLonInKm(data[i].lat, data[i].lng, req.body.lat, req.body.lng);
      console.log(distanceInKilometer);
      if (distanceInKilometer < req.body.radius) {
        placesAroundYour.push(data[i]);
      }
    }

    return res.json({
      message: "Places Around You",
      success: true,
      data: placesAroundYour
    });
  });
}