let express = require('express');

let router = express.Router();

let itemCrtl = require('./Item.controller');

// router.get('/radius', placeCrtl.getPlaceByRadius);

/**
 * @api {post} /item  Request Get Item
 * @apiName registerItem
 * @apiGroup Item
 *
 * @apiParam {name} name Name of Item.
 * @apiParam {dtype} dtype of Food,Alochol.
 * @apiSuccessExample {json} Success-Response:
 *    HTTP/1.1 200 OK
 *     {
 *       "message:"newItem Has Been Added",
 *       "success": true,
 *        "data": {
 *            "updatedAt": "2019-03-21T20:19:55.843Z",
 *            "createdAt": "2019-03-21T20:19:55.843Z",
 *            "name": "Nanga nach2",
 *            "dtype": "Food",
 *            "_id": "5c93f1eb3516fb0114824bda"                   
 *     }
 * 
 */

router.post('/', itemCrtl.registerItem);
// router.get('/', placeCrtl.getPlaces);


module.exports = router;
