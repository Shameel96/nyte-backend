let express = require('express');

let router = express.Router();

let placeCrtl = require('./places.controller');

router.get('/radius', placeCrtl.getPlaceByRadius);
router.post('/', placeCrtl.registerPlaces);
router.get('/', placeCrtl.getPlaces);


module.exports = router;
